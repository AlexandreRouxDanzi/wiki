{% assign device = site.data.devices[page.device] %}

# Install /e/ on a {{ device.vendor }} {{ device.name }} - "{{ device.codename }}" {%- if device.is_beta %} (beta) {%- endif %}

{% include warning_read_before.md %}

{% if device.before_install %}
{% capture before_install %}templates/{{ device.before_install }}.md{% endcapture %}
{% include {{ before_install }} %}
{% endif %}

{% include requirements.md %}

{% if device.recovery_install_method %}
{% capture recovery_install_method %}templates/recovery_install_{{ device.recovery_install_method }}.md{% endcapture %}
{% include {{ recovery_install_method }} %}
{% endif %}

{% include install_e.md %}

{% if device.is_lineageos_official %}
{% include licence_device.md %}
{% endif %}
