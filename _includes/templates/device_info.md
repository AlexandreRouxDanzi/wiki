{% assign device = site.data.devices[page.device] %}

# {{ device.vendor }} {{ device.name }} - "{{ device.codename }}" {%- if device.is_beta %} (beta) {%- endif %}

## Device informations

- Vendor: **{{ device.vendor }}**
- Name: **{{ device.name }}**
- Codename: **{{ device.codename }}**
{%- if device.models %}
- models:
{%- for model in device.models %}
    - {{ model }}
{%- endfor %}
{%- endif %}

## Download


{%- for status in device.status %}
- [{{status}}](https://images.ecloud.global/{{status}}/{{device.codename}}/) ({{device.versions}})
{%- endfor %}

## Install

Click [here]({{ "devices/" | append: device.codename | append: "/install" | relative_url }}) to learn how to install /e/ on a {{ device.vendor }} {{ device.name }} - "{{ device.codename }}"

## Build

Click [here]({{ site.baseurl }}/how-tos/build-e) to learn how to build /e/ for a {{ device.vendor }} {{ device.name }} - {{ device.codename }}"

## What's not (yet) working

Please find issues opened for this device [here](https://gitlab.e.foundation/groups/e/-/boards/12?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D={{device.codename}})

Have you found a bug? Please report here (documentation)
