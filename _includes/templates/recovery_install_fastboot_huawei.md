## Unlocking the bootloader

{% if device.is_unlockable == false %}
**Warning** : Huawei does not provide unlock password anymore (see here). Consequently, if your device is not unlocked yet, you will not be able to install /e/ on it, unless using unofficial ways.
{% else %}

{% include alerts/warning_unlocking_bootloader.md %}

1. Enable OEM unlock in the Developer options under device Settings
1. Visit [Huawei’s official unlocking website](https://hwid1.vmall.com/CAS/portal/loginAuth.html?service=http%3A%2F%2Fwww.emui.com%2Fplugin.php%3Fid%3Dhwlogin%3Avalidate%26ru%3Dhttp%253A%252F%252Fwww.emui.com%252Fplugin.php%253Fid%253Dunlock%2526mod%253Dunlock%2526action%253Dapply&loginChannel=22000000&reqClientType=22&deviceID=&lang=en-us&InviterUserID=&Inviter=&isFromPC=1&loginUrl=https%3A%2F%2Fhwid1.vmall.com%2Foauth2%2Fportal%2Fcloud_login.jsp&adUrl=http%3A%2F%2Fwww.emui.com%2Fsource%2Fplugin%2Fhwlogin%2Flogin.php&viewType=1&isDialog=0&themeName=cloudTheme), you’ll be asked to login first.
1. Follow the instructions and get your unlock password.
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:

    ```shell
    fastboot devices
    ```
1. Now type the following command to unlock the bootloader:

    ```shell
    fastboot oem unlock ****************
    ```
    Replace ****** with the 16-digit unique unlock password that was obtained in step 2.
1. Wait for the bootloader unlocking process to complete. Once finished, your device will reboot to system and will be restored to factory settings. If an incorrect password is entered, a message will be displayed and your device will also reboot to system.
1. Since the device resets completely, you will need to re-enable USB debugging to continue.
1. To check the bootloader lock status, reboot the device to fastboot mode (repeating steps 3 and 4) and type:

    ```shell
    fastboot oem get-bootinfo
    ```
    The message `Bootloader Lock State: LOCKED` indicates that the bootloader is still locked. Perform the unlocking procedure again and check that the password was entered correctly. On the other hand, the message `Bootloader Lock State: UNLOCKED` indicates that the bootloader has been unlocked. You can now install third-party firmware.
{% endif %}

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
