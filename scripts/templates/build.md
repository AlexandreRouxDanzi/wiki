---
sidebar: home_sidebar
title: Build /e/ for {vendor} {name} - {codename}
folder: build
layout: default
permalink: /devices/{codename}/build
device: {codename}
---
{% include templates/device_build.md %}
