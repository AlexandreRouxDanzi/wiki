---
sidebar: home_sidebar
title: Install /e/ on {vendor} {name} - {codename}
folder: install
layout: default
permalink: /devices/{codename}/install
device: {codename}
---
{% include templates/device_install.md %}
