---
layout: page
title: /e/ product description - a pro-privacy Android ROM and online services
permalink: what-s-e
search: exclude
---

## What's in /e/?

/e/ is an a mobile ecosystem (ROM + onlines services) that:
- is **open source**
- is **pro-privacy**
- is compatible with most existing **Android applications**
- cares about **usability**
- already exists!

It's the **alternative to the Apple/Google duopoly** on the smartphone.

It consists of:
*  an installable mobile operating system for smartphones, which is forked from Android and strongly "ungoogled"
*  a set of sorted and improved default open source applications
*  various online services that are linked to the mobile operating system, such as: a meta search engine for the web, drive (with synchronization), mail, calendar, notes, tasks.

## 1. Description of the /e/ ROM (installable mobile operating system)

The /e/ ROM is a fork of Android (and in particular the LineageOS flavor of Android). At the moment, two versions are supported: Android Nougat and Android Oreo. Android Pie will be supported in a near future.

### "Ungoogling"

- Google default search engine is removed everywhere and replaced by other services (see below in default apps and services)
- Google Services are replaced by microG and alternative services (see below for more details)
- DNS settings can be enforced to a specific server
- Geolocation is using Mozilla Location Services in addition to GPS
- CalDAV/CardDAV management and synchronization application (DAVDroid) is fully integrated with the user account and calendar/contact application
- Google servers are not used anymore to check connectivity (ongoing)

### User interface

![Capture_d_écran_2019-04-30_à_15.05.19](images/Capture_d_écran_2019-04-30_à_15.05.19.png)

- A new launcher "BlissLauncher" was developed to offer an attractive look & feel. It includes a set of icons that were designed for the project. It has application widget support and is using /e/ search engine by default.
- Changes have been done in settings to offer a better looking interface

### Services

- A specific file synchronization service has been developed for multimedia and file contents. It can use [/e/ cloud services server at https://ecloud.global](https://ecloud.global) or [self-hosted /e/ cloud service](https://gitlab.e.foundation/e/infra/ecloud-selfhosting)s (beta).
- The weather provider is the LOS weather provider using data fetched from Open Weather Map
- Account Manager has been modified to support /e/ user accounts, with a single identity, thus providing /e/ email, /e/ sync, /e/ calendar, /e/ contacts, /e/ notes, /e/ tasks to users. /e/ cloud server is either at ecloud.global, or self-hosted by the user (ongoing)

## 2. Description of /e/ default applications

All pre-installed applications are open source applications but the Maps app (read details about this in the FAQ).

- Web-browser: an an ungoogled fork of Chromium, built from Bromite patch-sets with specific /e/ settings
- Mail: a fork of K9-mail for better user experience, some bugfixes and support for oauth, with OpenKeyChain for PGP encryption support
- SMS: a fork of QKSMS
- Camera: OpenCamera
- Dialer: default Android dialer application
- Calendar: a fork of Etar calendar, that fixes crashes with calendar invitations
- Contact: default Android contact application
- Clock: a fork of Android deskclock application
- Gallery: default Android gallery3d application
- Filemanager: Android documentsui application
- Sound recorder: default LineageOS sound recorder
- Calculator: default Android calculator2 application
- Keyboard: default Android keyboard
- Android application installer is /e/ application installer

Other preinstalled applications:
- Maps: MagicEarth (read the FAQ about its license)
- Weather: a fork of GoodWeather for a modified user interface, using data fetched from Open Weather Map
- PDF reader: MuPDF
- Notes: a fork of NextCloud Notes to support /e/ online accounts
- Tasks: OpenTasks
- Light: a fork of Simple-FlashLight

## 3. Description of /e/ online services at ecloud.global

![Capture_d_écran_2019-04-30_à_14.33.38](images/Capture_d_écran_2019-04-30_à_14.33.38.png)

- /e/ "spot" at https://spot.ecloud.global search engine is a fork of SearX meta-search engine, with major look&feel improvements and peformance improvements
- /e/ online drive, mail, calendar, contacts, notes, tasks is using NextCloud, Postfix and Dovecot. It's been integrated to offer a single login identity. Users can retrieve their data online at [https://ecloud.global](https://ecloud.global) services, or [self-hosted](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) on their own server (ongoing)

# /e/ Development status

## Status

## Next majors milestones:
- in the future /e/ will be available in two versions:
  - standard /e/ with default applications
  - /e/ with minimal number of default/pre-installed applications

## Status of ungoogling & pro-privacy features

- we are currently [addressing a number of issues with ungoogling](https://gitlab.e.foundation/search?group_id=&project_id=&repository_ref=&scope=issues&search=Infosec+Handbook+Review)

## What to expect in futher releases?

In 2019 we also plan to:
- introduce more pro-privacy features (VPN...)
- add the capability to uninstall most pre-installed applications
- add several user profile to impact the choice of default applications installed during first-time usage wizard
- add Progressive Web Apps to the application repository, and corresponding Google-free SDKs for app publishers, because we believe that web apps is the future for most mobile applications.

# Want to give /e/ a try?

[Install it now](devices)!
