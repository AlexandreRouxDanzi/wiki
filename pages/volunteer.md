---
layout: page
title: Volunteers @/e/
permalink: volunteers
search: exclude
---

## Want to become a Volunteer and help /e/:

 - Would you like to introduce /e/ to the world
 - Support /e/ at local events around user and data privacy
 - Host an install party in your locality
 - or even submit your ideas to reach more people around the world.

If you felt like checking one or more of the above options then do get in touch with us.

## How to apply?

[Contact us](https://e.foundation/contact/)