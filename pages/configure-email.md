---
layout: page
title: Configure /e/ mail@/e/
permalink: how-tos/configure-email
search: exclude
---

## Email Configuration

 - You can use your @e.email email account to log in at: [https://e.email](https://e.email) or [https://ecloud.global/](https://ecloud.global/)  

Lots of different email clients are able to automatically configure your email account – you just need to provide email address and password in order to connect.

- For example:
/e/ Mail, Mozilla Thunderbird 3.1 or newer, Evolution, KMail, Kontact and Outlook 2007 or newer.

- If you are using a current Apple Mail version (both on macOS or iOS) you can visit [https://autoconfig.ecloud.global/](https://autoconfig.ecloud.global/) and fill in your details to download a configuration package that automatically configures your client.


## Manual Configuration

For manual configuration, please use the following settings:


   - SMTP/IMAP server: mail.ecloud.global

   - SMTP/IMAP username: your /e/ email address in full (example: john.doe@e.email)

   - SMTP/IMAP password: your /e/ email password

   - SMTP port: 587, plain password, STARTTLS, requires authentication

   - IMAP port: 993, SSL/TLS, requires authentication
