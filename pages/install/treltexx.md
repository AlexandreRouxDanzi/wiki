---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy Note 4 (Exynos LTE) - treltexx
folder: install
layout: default
permalink: /devices/treltexx/install
device: treltexx
---
{% include templates/device_install.md %}
