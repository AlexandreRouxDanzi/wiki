---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto G - falcon
folder: install
layout: default
permalink: /devices/falcon/install
device: falcon
---
{% include templates/device_install.md %}
