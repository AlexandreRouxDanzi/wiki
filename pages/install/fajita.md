---
sidebar: home_sidebar
title: Install /e/ on OnePlus 6T - fajita
folder: install
layout: default
permalink: /devices/fajita/install
device: fajita
---
{% include templates/device_install.md %}
