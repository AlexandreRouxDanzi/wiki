---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S6 edge - zeroltexx
folder: install
layout: default
permalink: /devices/zeroltexx/install
device: zeroltexx
---
{% include templates/device_install.md %}
