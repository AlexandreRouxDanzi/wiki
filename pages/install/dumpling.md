---
sidebar: home_sidebar
title: Install /e/ on OnePlus 5T - dumpling
folder: install
layout: default
permalink: /devices/dumpling/install
device: dumpling
---
{% include templates/device_install.md %}
