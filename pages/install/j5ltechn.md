---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy J5 LTE (SM-J5008) - j5ltechn
folder: install
layout: default
permalink: /devices/j5ltechn/install
device: j5ltechn
---
{% include templates/device_install.md %}
