---
sidebar: home_sidebar
title: Install /e/ on Google Nexus 6P - angler
folder: install
layout: default
permalink: /devices/angler/install
device: angler
---
{% include templates/device_install.md %}
