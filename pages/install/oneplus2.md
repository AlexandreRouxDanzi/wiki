---
sidebar: home_sidebar
title: Install /e/ on OnePlus 2 - oneplus2
folder: install
layout: default
permalink: /devices/oneplus2/install
device: oneplus2
---
{% include templates/device_install.md %}
