---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto E - condor
folder: install
layout: default
permalink: /devices/condor/install
device: condor
---
{% include templates/device_install.md %}
