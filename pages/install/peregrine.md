---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto G 4G - peregrine
folder: install
layout: default
permalink: /devices/peregrine/install
device: peregrine
---
{% include templates/device_install.md %}
