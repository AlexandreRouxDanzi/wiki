---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy J5 LTE - j5lte
folder: install
layout: default
permalink: /devices/j5lte/install
device: j5lte
---
{% include templates/device_install.md %}
