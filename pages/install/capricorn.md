---
sidebar: home_sidebar
title: Install /e/ on Xiaomi Mi 5s - capricorn
folder: install
layout: default
permalink: /devices/capricorn/install
device: capricorn
---
{% include templates/device_install.md %}
