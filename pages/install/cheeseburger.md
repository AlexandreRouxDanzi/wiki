---
sidebar: home_sidebar
title: Install /e/ on OnePlus 5 - cheeseburger
folder: install
layout: default
permalink: /devices/cheeseburger/install
device: cheeseburger
---
{% include templates/device_install.md %}
