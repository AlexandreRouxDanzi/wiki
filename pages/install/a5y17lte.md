---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy A5 (2017) - a5y17lte
folder: install
layout: default
permalink: /devices/a5y17lte/install
device: a5y17lte
---
{% include templates/device_install.md %}
