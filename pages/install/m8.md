---
sidebar: home_sidebar
title: Install /e/ on HTC One (M8) - m8
folder: install
layout: default
permalink: /devices/m8/install
device: m8
---
{% include templates/device_install.md %}
