---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy Note 2 (LTE) - t0lte
folder: install
layout: default
permalink: /devices/t0lte/install
device: t0lte
---
{% include templates/device_install.md %}
