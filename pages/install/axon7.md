---
sidebar: home_sidebar
title: Install /e/ on ZTE Axon7 - axon7
folder: install
layout: default
permalink: /devices/axon7/install
device: axon7
---
{% include templates/device_install.md %}
