---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy A3 (2016 Exynos) - a3xeltexx
folder: install
layout: default
permalink: /devices/a3xeltexx/install
device: a3xeltexx
---
{% include templates/device_install.md %}
