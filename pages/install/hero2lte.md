---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S7 edge - hero2lte
folder: install
layout: default
permalink: /devices/hero2lte/install
device: hero2lte
---
{% include templates/device_install.md %}
