---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto G4 Play - harpia
folder: install
layout: default
permalink: /devices/harpia/install
device: harpia
---
{% include templates/device_install.md %}
