---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S7 - herolte
folder: install
layout: default
permalink: /devices/herolte/install
device: herolte
---
{% include templates/device_install.md %}
