---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S4 Mini (International LTE) - serranoltexx
folder: install
layout: default
permalink: /devices/serranoltexx/install
device: serranoltexx
---
{% include templates/device_install.md %}
