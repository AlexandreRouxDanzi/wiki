---
sidebar: home_sidebar
title: Install /e/ on Xiaomi Pocophone F1 - beryllium
folder: install
layout: default
permalink: /devices/beryllium/install
device: beryllium
---
{% include templates/device_install.md %}
