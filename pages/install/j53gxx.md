---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy J5 3G - j53gxx
folder: install
layout: default
permalink: /devices/j53gxx/install
device: j53gxx
---
{% include templates/device_install.md %}
