---
sidebar: home_sidebar
title: Install /e/ on LeEco Le 2 - s2
folder: install
layout: default
permalink: /devices/s2/install
device: s2
---
{% include templates/device_install.md %}
