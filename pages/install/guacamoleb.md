---
sidebar: home_sidebar
title: Install /e/ on OnePlus 7 - guacamoleb
folder: install
layout: default
permalink: /devices/guacamoleb/install
device: guacamoleb
---
{% include templates/device_install.md %}
