---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy J5N LTE - j5nlte
folder: install
layout: default
permalink: /devices/j5nlte/install
device: j5nlte
---
{% include templates/device_install.md %}
