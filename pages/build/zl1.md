---
sidebar: home_sidebar
title: Build /e/ for LeEco LePro3/LePro3 Elite - zl1
folder: build
layout: default
permalink: /devices/zl1/build
device: zl1
---
{% include templates/device_build.md %}
