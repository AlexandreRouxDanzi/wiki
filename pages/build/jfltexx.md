---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S4 - jfltexx
folder: build
layout: default
permalink: /devices/jfltexx/build
device: jfltexx
---
{% include templates/device_build.md %}
