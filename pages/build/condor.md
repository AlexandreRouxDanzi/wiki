---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto E - condor
folder: build
layout: default
permalink: /devices/condor/build
device: condor
---
{% include templates/device_build.md %}
