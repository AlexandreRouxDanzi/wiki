---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S9 - starlte
folder: build
layout: default
permalink: /devices/starlte/build
device: starlte
---
{% include templates/device_build.md %}
