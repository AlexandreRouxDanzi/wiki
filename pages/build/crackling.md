---
sidebar: home_sidebar
title: Build /e/ for Wileyfox Swift - crackling
folder: build
layout: default
permalink: /devices/crackling/build
device: crackling
---
{% include templates/device_build.md %}
