---
sidebar: home_sidebar
title: Build /e/ for Sony Xperia XA2 - pioneer
folder: build
layout: default
permalink: /devices/pioneer/build
device: pioneer
---
{% include templates/device_build.md %}
