---
sidebar: home_sidebar
title: Build /e/ for OnePlus 5T - dumpling
folder: build
layout: default
permalink: /devices/dumpling/build
device: dumpling
---
{% include templates/device_build.md %}
