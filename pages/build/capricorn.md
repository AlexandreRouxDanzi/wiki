---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi 5s - capricorn
folder: build
layout: default
permalink: /devices/capricorn/build
device: capricorn
---
{% include templates/device_build.md %}
