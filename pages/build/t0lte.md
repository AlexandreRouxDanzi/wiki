---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 2 (LTE) - t0lte
folder: build
layout: default
permalink: /devices/t0lte/build
device: t0lte
---
{% include templates/device_build.md %}
