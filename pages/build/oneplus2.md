---
sidebar: home_sidebar
title: Build /e/ for OnePlus 2 - oneplus2
folder: build
layout: default
permalink: /devices/oneplus2/build
device: oneplus2
---
{% include templates/device_build.md %}
