---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J5 3G - j53gxx
folder: build
layout: default
permalink: /devices/j53gxx/build
device: j53gxx
---
{% include templates/device_build.md %}
