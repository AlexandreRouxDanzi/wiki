---
sidebar: home_sidebar
title: Build /e/ for OnePlus 5 - cheeseburger
folder: build
layout: default
permalink: /devices/cheeseburger/build
device: cheeseburger
---
{% include templates/device_build.md %}
