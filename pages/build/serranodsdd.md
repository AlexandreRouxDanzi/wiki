---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S4 Mini (International Dual SIM) - serranodsdd
folder: build
layout: default
permalink: /devices/serranodsdd/build
device: serranodsdd
---
{% include templates/device_build.md %}
