---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 4 (Exynos LTE) - treltexx
folder: build
layout: default
permalink: /devices/treltexx/build
device: treltexx
---
{% include templates/device_build.md %}
