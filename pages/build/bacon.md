---
sidebar: home_sidebar
title: Build /e/ for OnePlus One - bacon
folder: build
layout: default
permalink: /devices/bacon/build
device: bacon
---
{% include templates/device_build.md %}
