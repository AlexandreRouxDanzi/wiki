---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J5 LTE (SM-J5008) - j5ltechn
folder: build
layout: default
permalink: /devices/j5ltechn/build
device: j5ltechn
---
{% include templates/device_build.md %}
