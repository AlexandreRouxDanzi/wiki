---
layout: page
title: Smartphones
permalink: devices/
search: exclude
---

## What's in /e/?

Please check the [/e/ product description](../what-s-e)

## How to install /e/ on your smartphone?

In order to install /e/:
* check that your Smartphone is in the supported list given below
* ensure that you have an /e/ account (for /e/ online services such as mail, drive, calendar...). You can register for a [free /e/ acount here](../create-an-ecloud-account)
* proceed to installation
* find some [community support](https://e.foundation/get-support/#help)
* [report issues here](issues)
* [report your experience here](testimonies)
* [contribute to the project!](projects-looking-for-contributors)

Can't find your Smartphone? Do a [device proposal here](https://community.e.foundation/c/e-devices/request-a-device) after checking it was not proposed earlier. Or do the work and become a device maintainer (send an email to [join@e.email](mailto:join@e.email), with a clear subject line (like: ROM Maintainer for device XYZ123).

## Smartphone build branch details

Note: the v0.x-n branch is based on Nougat, the v0.x-o branch is based on "Oreo", the v0.x-p branch is based on "Pie". There are no differences between the three in term of "/e/ features".

## Smartphones list

{% assign devices = "" | split: " " %}
{% for device in site.data.devices %}
{% assign devices = devices | push: device[1] %}
{% endfor %}

{% assign sorted = devices | sort_natural: 'name' | sort_natural: 'vendor' %}
{% assign lastVendor = "" %}
{% assign nbDevices = 0 %}

{%- for device in sorted %}
{%- assign nbDevices = nbDevices | plus:'1' %}
{%- if device.vendor != lastVendor %}
{%- assign lastVendor = device.vendor %}

### {{ lastVendor }}

| Vendor | Name | Codename |
|--------|------|----------|
{%- endif %}
| {{device.vendor}} | {{device.name}} {%- if device.is_beta %} (beta) {%- endif %} | [{{device.codename}}]({{device.codename}}) |

{%- endfor %}


Smartphones supported: {{ nbDevices }}
