---
layout: page
title: Build status
permalink: build-status
search: exclude
---

## Custom

Custom builds are any builds made by /e/ or external users, with possible changes to /e/ sources besides code developed for specific device support. Custom builds are not supported by /e/, and are not publicly available for download on /e/ websites.

## Unofficial

Unofficial builds are builds made from /e/ sources code, which are supposed to work on the target device. However they may still be in a validation phase, or they do not have yet a maintainer. They do not receive OTA updates. Unofficial builds are available for download on /e/ websites and are tagged as "UNOFFICIAL".

## Community

The level of quality for these builds is considered high, security updates are applied if possible, but there is not yet or no more official maintainer. Community builds have nightly builds and can be updated OTA. Source code have to be hosted on our GitLab instance, or on trusted sources (LineageOS GitHub group, AOSP).

## Official

Official builds are unofficial builds that have the reputation to work well enough and have a maintainer. Official builds are available for download on /e/ websites.
