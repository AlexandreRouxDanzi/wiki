---
layout: default
title: Report an issue
permalink: how-tos/report-an-issue
search: exclude
---

# Report an issue

What to do when you encounter an issue?

## 1. Connect to our GitLab server:

* If you have an account, [login](https://gitlab.e.foundation/users/sign_in)
* If not, [create one]()

## 2. Go to our issue board:

/e/ issues are listed here: https://gitlab.e.foundation/groups/e/-/issues

Before doing anything, please check if someone already created an issue about the problem you have encountered.

1. *"My problem is already described."* You can add some missing information to help developers: how you got the issue, steps to reproduce.
1. *"Nobody has reported my issue."* In this case, create a new one!

At the top right of the page, click **Select project to create an issue** and choose **e/management**. Then, click **New issue in management**.

![Capture_du_2018-09-10_12-31-38](images/Capture_du_2018-09-10_12-31-38.png)

> If you are sure that your issue is related to a specific project, please report the issue within the given project.

## 3. Create a new issue

Please copy/paste the following template, and fill it by adding the title and fill in each part with related information.

```markdown
## Summary
(Summarize the bug encountered, briefly and precisely)

## Steps to reproduce
(How one can reproduce the issue - this is very important)

## What is the current behavior?
(What actually happens)

## What is the expected correct behavior?
(What you should see instead)

## Relevant logs and/or screenshots
(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)
(You can use `adb logcat` command to capture log of you device, when connected to a computer)

## Possible fixes
(If you can, link to the line of code that might be the cause for this problem)

```

Once you have finished, you can create your issue by clicking **Submit issue**.

Source : https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests

## 4. Syntax

* [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)
* [Markdown Guide  (GitLab)](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
