---
sidebar: home_sidebar
title: Info about Xiaomi Pocophone F1 - beryllium
folder: info
layout: default
permalink: /devices/beryllium
device: beryllium
---
{% include templates/device_info.md %}
