---
layout: page
title: Testers @/e/
permalink: testers
search: exclude
---

## Volunteer as a tester (work remotely):

 - Do you like to flash ROM's and install applications on your android phone? Would you like to be a part of the team of volunteers who would test out the /e/ ROM of your device? Fancy yourself as a beta tester for applications developed and released by /e/? Then look no further send in a mail with the details as asked for below

## How to apply?

Please send an email to <testing@e.email> with the below details 

   - /e/ email ID : 
   - Telegram ID  : 
   - Device name  : 
   - Tester Name  : (optional)

## Why do we want these details?

   - /e/ email ID : To add your name to an email distribution list for testers. If you do not have an /e/ email ID that is ok, send across an email ID which you access
   
- Telegram ID  : To add your name to our testing channel. This has to be in the format @name. Phone numbers do not work while adding to the channel!
   - Device name  : The name of the device you want to test for. It should be on our supported list. Unofficial builds will not come under the scope of our testing cycle.
    
   - Tester Name  : Optional. Your name if you want to share it. 


